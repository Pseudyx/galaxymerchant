﻿using System.Linq;

namespace GalaxyMerchant.App
{
    public class Invoker
    {
        private Command _command;

        public void SetCommand(Command command)
        {
            this._command = command;
        }

        public void ExecuteCommand(string userCommand)
        {
            _command.Execute(userCommand);
        }

        public void ExecuteFromFile(string filePath)
        {
            FromFile.ReadCommands(filePath)?.ForEach(cmd => _command.Execute(cmd));
        }
    }
}
