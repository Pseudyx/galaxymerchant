﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GalaxyMerchant.App
{
    public static class FromFile
    {
        public static List<string> ReadCommands(string filePath)
        {
            if (CheckFile(filePath))
            {
                var fileContents = File.ReadAllLines(filePath);
                if(fileContents.Length == 0)
                {
                    Console.WriteLine("File found, but no content");
                }
                return fileContents.ToList();
            }

            return null;
        }

        private static bool CheckFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                if(filePath.Substring(filePath.LastIndexOf('.') + 1) != "txt")
                {
                    Console.WriteLine("Can only read from txt file");
                    return false;
                }
                return true;
            }
            else
            {
                Console.WriteLine("Can not find file");
                return false;
            }
        }
    }
}
