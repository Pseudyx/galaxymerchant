﻿using System.Text;
using System.Linq;
using System;

namespace GalaxyMerchant.App
{
    public class Program
    {
        static void Main(string[] args)
        {
            var calculator = new RomanCalculator();

            Receiver receiver = new TradeController(calculator);
            Command command = new ConcreteCommand(receiver);
            Invoker invoker = new Invoker();

            invoker.SetCommand(command);

            Init();

            string userCommand = GetCleanCommand();
            while (userCommand.ToUpper() != "EXIT")
            {
                if (userCommand.ToUpper() == "HELP" || userCommand == "/?")
                {
                    System.Console.Write(Help());
                }
                else if(userCommand.Split(' ')[0].ToUpper() == "FILE" && userCommand.Split(' ').Length > 1)
                {
                    var filePath = String.Join(" ", userCommand.Split(' ').Skip(1).ToArray());
                    invoker.ExecuteFromFile(filePath);
                }
                else
                {
                    invoker.ExecuteCommand(userCommand);
                }
                userCommand = GetCleanCommand();
            }
        }

        private static string GetCleanCommand()
        {
            System.Console.WriteLine();
            System.Console.Write("> ");
            return System.Console.ReadLine().Trim();
        }

        private static void Init()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("+=====================================+");
            sb.AppendLine("|    Merchant's Guide to the Galaxy   |");
            sb.AppendLine("+=====================================+");
            sb.AppendLine("RULES:");
            sb.AppendLine("       1: Always carry a towel");
            sb.AppendLine("       2: Don’t Panic!");
            sb.AppendLine("       3: Type Help or /? for commands");
            sb.AppendLine();

            System.Console.Write(sb.ToString());

        }

        private static string Help()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Roman Numerals:");
            sb.AppendLine("                             I = 1");//V X L C D M
            sb.AppendLine("                             V = 5");
            sb.AppendLine("                             X = 10");
            sb.AppendLine("                             L = 50");
            sb.AppendLine("                             C = 100");
            sb.AppendLine("                             D = 500");
            sb.AppendLine("                             M = 1000");
            sb.AppendLine();
            sb.AppendLine("COMMANDS:");
            sb.AppendLine("Add Galactic Numeral:        > {Name} is {Roman Numeral}");
            sb.AppendLine("Add Resource:                > {Galactic Numerals} {Resource Name} is {value} Credits");
            sb.AppendLine("Galactic Numeral To Number:  > how much is {Galactic Numerals} ?");
            sb.AppendLine("Credit value of Resource:    > How many Credits is {Galactic Numerals} {Resource Name} ?");
            sb.AppendLine("Read commands from file:     > file {path\\name.txt}");
            sb.AppendLine("Close:                       > exit");

            return sb.ToString();

        }
    }
}
