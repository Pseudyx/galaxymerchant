﻿namespace GalaxyMerchant.App
{
    public class ConcreteCommand : Command
    {
        public ConcreteCommand(Receiver receiver) :
          base(receiver)
        {
            receiver.MessageHandler += ReceiverMessageHandler;
        }

        public override void Execute(string userCommand)
        {
            receiver.Action(userCommand);
        }

        private void ReceiverMessageHandler(object sender, MessageEventArgs e)
        {
            System.Console.WriteLine(e.Message);
        }
    }
}
