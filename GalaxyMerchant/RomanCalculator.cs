﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GalaxyMerchant
{
    public enum Roman
    {
        I = 1,
        IV = 4,
        V = 5,
        IX = 9,
        X = 10,
        XL = 40,
        L = 50,
        C = 100,
        CD = 400,
        D = 500,
        CM = 900,
        M = 1000
    }

    public class RomanCalculator : ICalculator
    {
        private readonly string[] repeatableNumerals = { "I", "X", "C", "M" };
        private readonly char[] specialChars = { 'D', 'L', 'V' };

        public int RomanToArabic(string roman)
        {
            ValidateRomanNumerals(roman);

            char[] numerals = roman.ToArray();
            int total = 0;

            int current, previous = 0;
            char currentRoman, nextRoman, previousRoman = '\0';

            for (int i = 0; i < numerals.Length; i++)
            {
                currentRoman = numerals[i];
                
                previous = previousRoman != '\0' ? GetRomanValueByName(previousRoman.ToString()) : '\0';
                current = GetRomanValueByName(currentRoman.ToString());

                if(!(i+1 >= numerals.Length))
                {
                    nextRoman = numerals[i + 1];
                    ValidateNumeralOrder(currentRoman, nextRoman);
                }

                if (previous != 0 && current > previous)
                {
                    total = total - (2 * previous) + current;
                }
                else
                {
                    total += current;
                }

                previousRoman = currentRoman;
            }

            return total;
        }

        public string ArabicToRoman(int number)
        {
            var roman = new StringBuilder();

            foreach (var item in ((Roman[])Enum.GetValues(typeof(Roman))).OrderByDescending(x => (int)x))
            {
                while (number >= (int)item)
                {
                    roman.Append(item.ToString());
                    number -= (int)item;
                }
            }

            return roman.ToString();
        }

        public static int GetRomanValueByName(string name)
        {
            return (int)(Roman)Enum.Parse(typeof(Roman), name);
        }

        public static Roman GetRomanByName(string name)
        {
            return (Roman)Enum.Parse(typeof(Roman), name);
        }

        private void ValidateRomanNumerals(string romanNumber)
        {
            if (Regex.Match(romanNumber, @"[^IVXLCDM]$*").Success)
            {
                throw new ArgumentException("Invalid Roman Numerals");
            }

            repeatableNumerals.ToList().ForEach(numeral =>
            {
                if (Regex.Match(romanNumber, @"[" + numeral + "]{4}").Success)
                {
                    throw new ArgumentException("Roman Numerals I, X, C, and M can be repeated three times in succession, but no more");
                }

                if (Regex.Matches(romanNumber, Regex.Escape(numeral)).Count > 4)
                {
                    throw new ArgumentException("Roman Numerals I, X, C, and M may appear four times if the third and fourth are separated by a smaller value");
                }
            });
        }

        private void ValidateNumeralOrder(char current, char next)
        {
            if (specialChars.Contains(current) && current == next)
            {
                throw new ArgumentException("Roman Numerals D, L, and V can not be repeated");
            }

            if (GetRomanValueByName(next.ToString()) > GetRomanValueByName(current.ToString()))
            {
                if (current == 'I' && !(new[] { 'V', 'X' }.Contains(next)))
                {
                    throw new ArgumentException("Roman Numeral I can be subtracted from V and X only");
                }
                else if (current == 'X' && !(new[] { 'L', 'C' }.Contains(next)))
                {
                    throw new ArgumentException("Roman Numeral X can be subtracted from L and C only");
                }
                //This logical check is redundant due to D and M beaing the only 2 numerals greater than C
                //But is added for extendability, if there were ever numerals added greater than M
                else if (current == 'C' && !(new[] { 'D', 'M' }.Contains(next)))
                {
                    throw new ArgumentException("Roman Numeral C can be subtracted from D and M only");
                }

                if (specialChars.Contains(current))
                {

                    throw new ArgumentException("Roman Numerals D, L, and V can not be subtracted");
                }
            }
        }
    }
}
