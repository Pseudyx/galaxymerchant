﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GalaxyMerchant
{
    public class NumeralCollection<T>
    {
        protected Dictionary<string, T> _dictionary;

        public string[] Keys
        {
            get
            {
                return _dictionary.Keys.ToArray();
            }
        }

        public NumeralCollection()
        {
            this._dictionary = new Dictionary<string, T>();
        }

        public void Add(string key, T unitValue)
        {
            _dictionary.Add(key, unitValue);
        }

        public T GetValue(string key)
        {
            T value;
            if (!_dictionary.TryGetValue(key, out value))
            {
                throw new ArgumentException("No Key found for provided value");
            }
            return value;
        }

        public string ToValueString(string[] keys)
        {
            return String.Join("", keys.ToList().Select(word => _dictionary[word].ToString()).ToArray());
        }
    }
}
