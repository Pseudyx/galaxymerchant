﻿namespace GalaxyMerchant
{
    public interface ITradeController
    {
        void AddGalacticNumeral(string[] words);
        void AddResourceUnit(string[] words);
        void HowMuch(string[] words);
        void HowMany(string[] words);

    }
}
