﻿namespace GalaxyMerchant
{
    public interface ICalculator
    {
        int RomanToArabic(string roman);
        string ArabicToRoman(int number);
    }
}
