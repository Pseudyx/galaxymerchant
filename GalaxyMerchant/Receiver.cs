﻿using System;

namespace GalaxyMerchant
{
    public abstract class Receiver
    {
        public event EventHandler<MessageEventArgs> MessageHandler;

        public abstract void Action(string userCommand);

        protected virtual void OnSendMessage(MessageEventArgs e)
        {
            MessageHandler?.Invoke(this, e);
        }
    }

    public class MessageEventArgs : EventArgs
    {
        public string Message { get; set; }
    }
}
