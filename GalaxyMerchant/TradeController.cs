﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GalaxyMerchant
{
    [Flags]
    public enum CommandSwitch
    {
        Erorr = 0,
        Numeral = 1,
        Resource = 2,
        HowMuch = 3,
        HowMany = 4
    }
   
    public class TradeController : Receiver, ITradeController
    {
        private NumeralCollection<Roman> _galacticNumeral;
        private NumeralCollection<double> _resourceUnit;
        protected ICalculator _calculator;

        public TradeController(ICalculator calculator)
        {
            this._galacticNumeral = new NumeralCollection<Roman>();
            this._resourceUnit = new NumeralCollection<double>();
            this._calculator = calculator;
        }

        public override void Action(string userCommand)
        {
            var words = GetWords(userCommand);

            try
            {
                if (words.Length >= 3 && words[1] == "is")
                {
                    AddGalacticNumeral(words);
                    return;
                }

                if (Regex.Match(userCommand, @"(is [\d]+ credits)$", RegexOptions.IgnoreCase).Success)
                {
                    AddResourceUnit(words);
                    return;
                }

                if (Regex.Match(userCommand, @"\A^(how much is)\s(\w)+", RegexOptions.IgnoreCase).Success)
                {
                    HowMuch(words);
                    return;
                }

                if (Regex.Match(userCommand, @"\A^(how many credits is)\s(\w)+", RegexOptions.IgnoreCase).Success)
                {
                    HowMany(words);
                    return;
                }

                //if we got here, the command was not recognised
                throw new ArgumentException("Command not executed");
            }
            catch
            {
                MessageEventArgs ErrorArg = new MessageEventArgs();
                ErrorArg.Message = "I have no idea what you are talking about";
                OnSendMessage(ErrorArg);
            }
        }

        public void AddGalacticNumeral(string[] words)
        {
            _galacticNumeral.Add(words[0], RomanCalculator.GetRomanByName(words[2]));
        }

        public void AddResourceUnit(string[] words)
        {
            var numerals = _galacticNumeral.ToValueString(words.Take(words.Length - 4).ToArray());
            var resource = words[words.Length - 4];
            var credits = double.Parse(words[words.Length - 2]);

            int romanValue = _calculator.RomanToArabic(numerals);
            double unitValue = credits / romanValue;
            _resourceUnit.Add(resource, unitValue);
        }

        public void HowMuch(string[] words)
        {
            var numerals = _galacticNumeral.ToValueString(words.Skip(3).ToArray());
            var number = _calculator.RomanToArabic(numerals);

            MessageEventArgs HowMuchArg = new MessageEventArgs();
            HowMuchArg.Message = $"{ string.Join(" ", words.Skip(3)) } is { number }";
            OnSendMessage(HowMuchArg);
        }

        public void HowMany(string[] words)
        {
            var resource = words[words.Length - 1];
            var galacticNumerals = words.Skip(4).Take(words.Length - 5).ToArray();
            var roman = _galacticNumeral.ToValueString(galacticNumerals);
            var credits = _resourceUnit.GetValue(resource) * _calculator.RomanToArabic(roman);

            MessageEventArgs HowManyArg = new MessageEventArgs();
            HowManyArg.Message = $"{ string.Join(" ", galacticNumerals) } { resource } is { credits } Credits";
            OnSendMessage(HowManyArg);
        }

        private string[] GetWords(string command)
        {
            var matches = Regex.Matches(command, @"([a-zA-z/0-9]+)");
            var wordList = new List<string>();
            foreach (Match match in matches)
            {
                wordList.Add(match.Value);
            }
            return wordList.ToArray();
        }
    }
}
