# README #

## Assessment Overview ##

The project has been developed to demonstrate design patterns and ability to form a solution to 
provided instruction

Developed from a TDD approach starting with a Roman Numerals calculator as a bassis for the
business logic for the entire applciation, then working up to the UI.

The solution focused more on the function of exceptions themselves, and assume that the assmemet is looking
at the use of, instead of the actual exceptions themselves, as example, the 1 exception ("I have no idea what you are talking about") 
has been used as a response based on the provided Test Output in the instructions.

Tests are broken into the logical functions of the assignment:

### CalculatorTest ###

The Calculator Test casese are geared towards error handeling and exceptions for the supplied rules of the assesment, 
with  final cases testing output of the calculator.

Rules - Roman Numerals convert to Arabic (Integer) function Tests to satisfy Specification:

* Numbers are formed by combining symbols together and adding the values.
* The symbols "I", "X", "C", and "M" can be repeated three times in succession, but no more.
* They may appear four times if the third and fourth are separated by a smaller value.
* Add numbers when symbols are placed in order of value, starting with the largest values.
* "D", "L", and "V" can never be repeated. 
* Subtract from next symbol when smaller values precede larger values.
* Only one small-value symbol may be subtracted from any large-value symbol.
* "I" can be subtracted from "V" and "X" only.
* "X" can be subtracted from "L" and "C" only.
* "C" can be subtracted from "D" and "M" only.
* "V", "L", and "D" can never be subtracted.

### NumeralCollectionTest ###
simple tests are designed for the required functions to supprt adding dictionaries of galactic numerals and resource units.


### CommandTest ###
Command Test cases are designed to test the expected return based on the instructions provided input and expected output.
These tests are a bare minimum of what the UI requires to interact from the abrastract reciever class and event message handeling.

## Set up ##
The Solution was built in VS2017 - It should be backwards compatible, as its only useing system libraries.
The solution is seperated into 3 Projects:

* GalaxyMerchant - logic library
* GalaxyMerchant.Console - UI Project
* GalaxyMerchant.Test - Test cases 

### Solution Architecture ###
Command pattern: The solution demonstrates a Command pattern, to encapsulate a request as an object, so as later to later implement queueing or log requests, and supports undoable operations.

The 2 projects are seperated by abstraction through the command pattern so that the GalaxyMurchant library could be completely re-written without affecting the UI/console project. 

-- Extension: I would consider using Dependency Injection on a larger solutions, in such cases as injecting the calcualter into the Trade controller and the trade controller into the command classes --

### Configuration ###
You will need to restore NuGet packages before building, as I have used NUnit and NUnit3TestAdapter for Unit Testing in the GalaxyMurchant.Test 
project. (NUnit3TestAdapter is required to run unit tests in Visual Studio) and Costura.Fody in the GalaxyMurchant.Console to compile the 
GalaxyMurchant.dll into the GalaxyMurchantApp.exe output, so that project can be run as a single exe file without supproting files. 

Make sure that GalaxyMerchant.Console is startup project.

Once you have built the solution, the Compiled exe can be found in the bin folder of debug or release depending on what setting you ran build.

* \GalaxyMerchant.Console\bin\[Debug||Release]\GalaxyMerchantApp.exe

You can copy the exe file out of the directory to execute anywhere as the resources are compiled into the exe.

### Dependencies ###

* GalaxyMerchant.Console - references: GalaxyMerchant
* GalaxyMerchant.Test - references: GalaxyMerchant

GalaxyMurchant.Console and GalaxyMurchant.Test can run independantly of each other (no dependance) however I have turned on run tests on build.

## Running the App ##
The commands are as per the provided instructions:

* Add Galactic Numeral:	{Name} is {Roman Numeral}
* Add Resource:  {Galactic Numerals} {Resource Name} is {value} Credits
* Galactic Numeral To Number: how much is {Galactic Numerals} ?
* Credit value of Resource: How many Credits is {Galactic Numerals} {Resource Name} ?
* Read commands from file: file {path\name.txt}
* Close: exit
