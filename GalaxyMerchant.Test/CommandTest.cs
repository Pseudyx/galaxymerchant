﻿using NUnit.Framework;

namespace GalaxyMerchant.Test
{
    [TestFixture]
    public class CommandTest
    {
        protected Receiver receiver;
        protected string message;

        [SetUp]
        public void Init()
        {
            var calculator = new RomanCalculator();
            receiver = new TradeController(calculator);

            receiver.Action("glob is I");
            receiver.Action("prok is V");
            receiver.Action("pish is X");
            receiver.Action("tegj is L");

            receiver.MessageHandler += MessageHandler;
        }

        private void MessageHandler(object sender, MessageEventArgs e)
        {
            message = e.Message;
        }

        [TestCase("how much is plum", "I have no idea what you are talking about")]
        [TestCase("how much wood could a wood chuch chuch", "I have no idea what you are talking about")]
        public void CommandInputWithUnknownParamaterReturnsValidationMessage(string command, string expectedResponse)
        {
            receiver.Action(command);
            Assert.AreEqual(expectedResponse, message);
        }

        [TestCase("apple is I", "how much is apple", "apple is 1")]
        [TestCase("green is X", "how much is green green", "green green is 20")]
        public void CommandInputStoresValueAndReturnsExpectedMessage(string command1, string command2, string expectedResponse)
        {
            receiver.Action(command1);
            receiver.Action(command2);
            Assert.AreEqual(expectedResponse, message);
        }

        [TestCase("glob glob Silver is 34 Credits", "how many Credits is glob prok Silver ?", "glob prok Silver is 68 Credits")]
        [TestCase("glob prok Gold is 57800 Credits", "how many Credits is glob prok Gold ?", "glob prok Gold is 57800 Credits")]
        [TestCase("pish pish Iron is 3910 Credits", "how many Credits is glob prok Iron ?", "glob prok Iron is 782 Credits")]
        public void HomwManyCommandConvertsResourceValueToCreditValueAndReturnsExpectedMessage(string command1, string command2, string expectedResponse)
        {
            receiver.Action(command1);
            receiver.Action(command2);
            Assert.AreEqual(expectedResponse, message);
        }

        [TestCase("how much is pish tegj glob glob ?", "pish tegj glob glob is 42")]
        [TestCase("how much is pish glob glob ?", "pish glob glob is 12")]
        public void HowMuchCommandAddsNumeralsAndReturnsExpectedMessage(string command, string expectedResponse)
        {
            receiver.Action(command);
            Assert.AreEqual(expectedResponse, message);
        }
    }
}
