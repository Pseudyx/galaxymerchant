﻿using NUnit.Framework;
using System;

namespace GalaxyMerchant.Test
{
    [TestFixture]
    public class NumeralCollectionTest
    {
        protected NumeralCollection<Roman> _galacticNumeral;
        protected NumeralCollection<double> _ResourceUnit;

        [SetUp]
        public void Init()
        {
            _galacticNumeral = new NumeralCollection<Roman>();
            _ResourceUnit = new NumeralCollection<double>();

            _galacticNumeral.Add("plop", Roman.X);
            _ResourceUnit.Add("dirt", 1234);
        }

        [TestCase("glob", Roman.I)]
        public void GalacticNumeralAddedToDictionaryIsRetrievable(string key, Roman numeral)
        {
            _galacticNumeral.Add(key, numeral);
            Assert.Contains(key, _galacticNumeral.Keys);
        }

        [TestCase("Silver", 17)]
        public void ResourceNumeralAddedToDictionaryIsRetrievable(string key, double numeral)
        {
            _ResourceUnit.Add(key, numeral);
            Assert.Contains(key, _ResourceUnit.Keys);
        }

        [TestCase("glob")]
        public void WhenKeyDoesNotExistInDictionaryThrowException(string key)
        {
            var ex = Assert.Throws<ArgumentException>(() => _galacticNumeral.GetValue(key));
            Assert.That(ex.Message, Is.EqualTo("No Key found for provided value"));
        }

        [TestCase("plop", Roman.X)]
        public void GetGalacticNumeralKeyFromDictionarProvidesExpectedValuey(string key, Roman expectedRoman)
        {
            Assert.AreEqual(expectedRoman, _galacticNumeral.GetValue(key));
        }

        [TestCase("dirt", 1234)]
        public void GetResourceKeyFromDictionarProvidesExpectedValuey(string key, double expectedRoman)
        {
            Assert.AreEqual(expectedRoman, _ResourceUnit.GetValue(key));
        }
    }
}
