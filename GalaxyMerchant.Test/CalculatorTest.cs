﻿using NUnit.Framework;
using System;

namespace GalaxyMerchant.Test
{
    [TestFixture]
    public class CalculatorTest
    {
        protected ICalculator _calculator;

        [SetUp]
        public void Init()
        {
            _calculator = new RomanCalculator();
        }

        /// <summary>
        /// Roman Numerals convert to Arabic (Integer) function Tests to satisfy Specification:
        /// # Numbers are formed by combining symbols together and adding the values.
        /// # The symbols "I", "X", "C", and "M" can be repeated three times in succession, but no more.
        /// # They may appear four times if the third and fourth are separated by a smaller value.
        /// # Add numbers when symbols are placed in order of value, starting with the largest values.
        /// # "D", "L", and "V" can never be repeated. 
        /// # Subtract from next symbol when smaller values precede larger values.
        /// # Only one small-value symbol may be subtracted from any large-value symbol.
        /// # "I" can be subtracted from "V" and "X" only.
        /// # "X" can be subtracted from "L" and "C" only.
        /// # "C" can be subtracted from "D" and "M" only.
        /// # "V", "L", and "D" can never be subtracted.
        /// </summary>

        [TestCase("IVADMB")]
        public void WhenInputContainsNonRomanNumeralThrowException(string input)
        {
            var ex = Assert.Throws<ArgumentException>(() => _calculator.RomanToArabic(input));
            Assert.That(ex.Message, Is.EqualTo("Invalid Roman Numerals"));
        }

        [TestCase("MMMM")]
        [TestCase("CCCCC")]
        [TestCase("XXXX")]
        [TestCase("IIII")]
        public void WhenRomanNumeralInSuccessionMoreThanThriceThrowException(string input)
        {
            var ex = Assert.Throws<ArgumentException>(() => _calculator.RomanToArabic(input));
            Assert.That(ex.Message, Is.EqualTo("Roman Numerals I, X, C, and M can be repeated three times in succession, but no more"));
        }

        [TestCase("IXIIIVI")]
        [TestCase("CMCCCIC")]
        [TestCase("XCXIXXX")]
        [TestCase("MMDMMMVI")]
        public void WhenRomanNumeralInSuccessionWithSeperatorCountMoreThanFourThrowException(string input)
        {
            var ex = Assert.Throws<ArgumentException>(() => _calculator.RomanToArabic(input));
            Assert.That(ex.Message, Is.EqualTo("Roman Numerals I, X, C, and M may appear four times if the third and fourth are separated by a smaller value"));
        }

        [TestCase("DD")]
        [TestCase("LL")]
        [TestCase("VV")]
        public void WhenNumeralsDLVAreRepeatedThrowException(string input)
        {
            var ex = Assert.Throws<ArgumentException>(() => _calculator.RomanToArabic(input));
            Assert.That(ex.Message, Is.EqualTo("Roman Numerals D, L, and V can not be repeated")); ;
        }

        [TestCase("IL")]
        [TestCase("VIL")]
        [TestCase("XD")]
        public void WhenLargerNumeralIsNonSubtractableBySmallerNumeralThrowException(string input)
        {
            var ex = Assert.Throws<ArgumentException>(() => _calculator.RomanToArabic(input));
            Assert.That(ex.Message, Is.EqualTo("Roman Numeral I can be subtracted from V and X only")
                                   .Or.EqualTo("Roman Numeral X can be subtracted from L and C only")
                                   .Or.EqualTo("Roman Numeral C can be subtracted from D and M only"));
        }

        [TestCase("DM")]
        [TestCase("LC")]
        [TestCase("VX")]
        public void WhenSmallerNumeralIsNonSubtractableNumeralThrowException(string input)
        {
            var ex = Assert.Throws<ArgumentException>(() => _calculator.RomanToArabic(input));
            Assert.That(ex.Message, Is.EqualTo("Roman Numerals D, L, and V can not be subtracted"));
        }

        [TestCase("M", 1000)]
        [TestCase("I", 1)]
        public void WhenInputContainSingleNumeralProvidesExpectedValue(string input, int expectedResult)
        {
            var returnValue = _calculator.RomanToArabic(input);
            Assert.AreEqual(expectedResult, returnValue);
        }

        [TestCase("MCMIII", 1903)]
        [TestCase("CMXCIX", 999)]
        public void WhenInputConvertsToExpectedValue(string input, int expectedResult)
        {
            var returnValue = _calculator.RomanToArabic(input);
            Assert.AreEqual(expectedResult, returnValue);
        }

        [TestCase(275, "CCLXXV")]
        [TestCase(1903, "MCMIII")]
        public void ArabicToRomanExpectedValue(int input, string expectedResult)
        {
            var returnValue = _calculator.ArabicToRoman(input);
            Assert.AreEqual(expectedResult, returnValue);
        }

    }
}
